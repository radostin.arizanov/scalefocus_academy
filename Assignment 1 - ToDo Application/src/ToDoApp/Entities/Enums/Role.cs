﻿namespace ToDoApp.Entities.Enums
{
    public enum Role
    {
        Admin,
        RegularUser
    }
}
