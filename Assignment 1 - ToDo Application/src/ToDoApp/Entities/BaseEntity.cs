﻿using System;

namespace ToDoApp.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public int CreatorId { get; set; }

        public DateTime LastChangeAt { get; set; }

        public int LastEditorId { get; set; }
    }
}
