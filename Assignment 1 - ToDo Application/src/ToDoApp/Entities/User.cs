﻿using System.Collections.Generic;

using ToDoApp.Entities.Enums;

namespace ToDoApp.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            SharedToDoLists = new List<ToDoList>();
        }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Role Role { get; set; }

        public List<ToDoList> SharedToDoLists { get; set; }
    }
}
