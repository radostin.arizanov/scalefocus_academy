﻿namespace ToDoApp.Entities
{
    public class ToDoList : BaseEntity
    {
        public string Title { get; set; }
    }
}
