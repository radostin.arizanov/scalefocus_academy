﻿using System;
using System.Linq;
using System.Collections.Generic;

using ToDoApp.Data;
using ToDoApp.Entities;

namespace ToDoApp.Services
{
    public class ToDoListService
    {
        //The name of file where lists are stored
        private const string ListsStrorageFileName = "Lists.json";
        //Database storage
        private readonly FileStorage _storage;
        //Application lists
        private readonly List<ToDoList> _appLists = new List<ToDoList>();

        public ToDoListService()
        {
            _storage = new FileStorage();
            List<ToDoList> listsFromFile = _storage.Read<List<ToDoList>>(ListsStrorageFileName);

            if (listsFromFile == null)
            {
                SeedData();
            }
            else
            {
                _appLists = listsFromFile;
            }
        }

        //Add default TODO List
        private void SeedData()
        {
            ToDoList list = new ToDoList()
            {
                Id = 1
            };

            _appLists.Add(list);
            SaveToFile();
        }

        //Save data to storage
        private void SaveToFile()
        {
            _storage.Write(ListsStrorageFileName, _appLists);
        }

        //Return all todo lists
        public List<ToDoList> GetAllLists()
        {
            List<ToDoList> lists = _appLists;
            return lists;
        }

        //Create todo list
        public bool CreateList(User user, string title)
        {
            //Check for already existing list with this title
            if (_appLists.Any(l => l.Title == title))
            {
                return false;
            }

            //Generate id for todo list
            int count = _appLists.Count;
            int id;

            if (count == 0)
            {
                id = 1;
            }
            else
            {
                id = _appLists[_appLists.Count - 1].Id + 1;
            }

            DateTime dateTime = DateTime.UtcNow;

            ToDoList list = new ToDoList()
            {
                Id = id,
                CreatorId = user.Id,
                Title = title,
                CreatedAt = dateTime,
                LastChangeAt = dateTime,
                LastEditorId = user.Id
            };

            _appLists.Add(list);
            SaveToFile();
            return true;
        }

        //Delete todo list by given user and todo list id
        public bool DeleteListById(User user, int id)
        {
            //Check if list exists
            ToDoList list = _appLists.FirstOrDefault(l => l.Id == id);

            if (list == null)
            {
                return false;
            }

            //Remove list from todo list storage
            if (list.CreatorId == user.Id)
            {
                _appLists.Remove(list);
                SaveToFile();
                return true;
            }

            //Remove list from user shared list
            if (user.SharedToDoLists.Contains(list))
            {
                user.SharedToDoLists.Remove(list);
                return true;
            }
            
            return false;
        }

        //Edit Todo list by given arguments
        public bool EditListById(int id, int newId, string newTitle, User user)
        {
            ToDoList list = _appLists.FirstOrDefault(l => l.Id == id);

            //Check if list exists and if the new id is already taken
            if (list == null || _appLists.Any(l => l.Id == newId))
            {
                return false;
            }

            list.Id = newId;
            list.Title = newTitle;
            list.LastEditorId = user.Id;
            list.LastChangeAt = DateTime.UtcNow;

            SaveToFile();
            return true;
        }

        //Return todo list by given id
        public ToDoList GetListById(int listId)
        {
            ToDoList list = _appLists.FirstOrDefault(l => l.Id == listId);
            return list;
        }
    }
}
