﻿using ToDoApp.Data;
using ToDoApp.Entities;
using ToDoApp.Entities.Enums;

using System;
using System.Linq;
using System.Collections.Generic;

namespace ToDoApp.Services
{
    public class UserService
    {
        //The name of file where users are stored
        private const string UsersStrorageFileName = "Users.json";
        //Database storage
        private readonly FileStorage _storage;
        //Application users
        private readonly List<User> _appUsers = new List<User>();
        //Current logged user
        public User CurrentUser { get; private set; }

        public UserService()
        {
            _storage = new FileStorage();
            List<User> usersFromFile = _storage.Read<List<User>>(UsersStrorageFileName);

            if (usersFromFile == null)
            {
                CreateAdmin();
            }
            else
            {
                _appUsers = usersFromFile;
            }
        }

        //Add admin in Users if there are no users in storage
        public void CreateAdmin()
        {
            User admin = new User
            {
                Id = 1,
                Username = "admin",
                Password = "adminpassword",
                Role = Role.Admin
            };

            _appUsers.Add(admin);
            SaveToFile();
        }

        public bool CreateUser(string username, string password, string firstName, string lastName, Role role)
        {
            //Check for already existing one
            if (_appUsers.Any(u => u.Username == username))
            {
                return false;
            }

            //Create unique id
            int newId = _appUsers[_appUsers.Count - 1].Id + 1;
            DateTime dateTime = DateTime.UtcNow;

            User newUser = new User
            {
                Id = newId,
                CreatedAt = dateTime,
                CreatorId = CurrentUser.Id,
                LastChangeAt = dateTime,
                LastEditorId = CurrentUser.Id,
                Username = username,
                Password = password,
                FirstName = firstName,
                LastName = lastName,
                Role = role,
            };

            _appUsers.Add(newUser);
            SaveToFile();
            return true;
        }

        //Save data to user storage
        private void SaveToFile()
        {
            _storage.Write(UsersStrorageFileName, _appUsers);
        }

        public bool Login(string username, string password)
        {
            User user = _appUsers.FirstOrDefault(u => u.Username == username);

            if (user != null && user.Password == password)
            {
                CurrentUser = user;
                return true;
            }

            return false;
        }

        public void LogOut()
        {
            CurrentUser = null;
        }

        //Edit User information by given arguments
        public bool EditUser(int id, string newUsername, string newPassword, string newFirstName, string newLastName)
        {
            User user = GetUserById(id);

            if (user == null)
            {
                return false;
            }

            user.Username = newUsername;
            user.Password = newPassword;
            user.FirstName = newFirstName;
            user.LastName = newLastName;
            user.LastChangeAt = DateTime.UtcNow;
            user.LastEditorId = CurrentUser.Id;

            SaveToFile();
            return true;
        }

        //Return User by id
        public User GetUserById(int id)
        {
            User user = _appUsers.FirstOrDefault(u => u.Id == id);
            return user;
        }

        //Delete User by give id
        public bool DeleteUser(int id)
        {
            User user = _appUsers.FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                return false;
            }

            _appUsers.Remove(user);
            SaveToFile();
            return true;
        }

        //Return all Users from storage
        public List<User> GetAllUsers()
        {
            List<User> users = _appUsers;
            return users;
        }

        //Add todo list which is shared by another user
        public void AddListInShare(ToDoList list, User receiver)
        {
            receiver.SharedToDoLists.Add(list);
            SaveToFile();
        }
    }
}
