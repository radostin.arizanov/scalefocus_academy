﻿using System;
using System.Linq;
using System.Collections.Generic;

using ToDoApp.Data;
using ToDoApp.Entities;

namespace ToDoApp.Services
{
    public class TaskService
    {
        //The name of file where tasks are stored
        private const string TasksStrorageFileName = "Tasks.json";
        //Database storage
        private readonly FileStorage _storage;
        //Application tasks
        private readonly List<Task> _appTasks = new List<Task>();

        public TaskService()
        {
            _storage = new FileStorage();
            List<Task> tasksFromFile = _storage.Read<List<Task>>(TasksStrorageFileName);

            if (tasksFromFile == null)
            {
                SeedData();
            }
            else
            {
                _appTasks = tasksFromFile;
            }
        }

        //Add default TODO List
        private void SeedData()
        {
            Task task = new Task()
            {
                Id = 1
            };

            _appTasks.Add(task);
            SaveToFile();
        }

        //Save data to storage
        private void SaveToFile()
        {
            _storage.Write(TasksStrorageFileName, _appTasks);
        }

        //Return all tasks
        public List<Task> GetAllTasks()
        {
            List<Task> tasks = _appTasks;
            return tasks;
        }

        //Return task by Id
        public Task GetTaskById(int id)
        {
            Task task = _appTasks.FirstOrDefault(t => t.Id == id);
            return task;
        }

        //Create Task
        public bool CreateTask(User user, int todoListId, string title, string description, bool isComplete)
        {
            //Check for already existing list with this title
            if (_appTasks.Any(t => t.Title == title))
            {
                return false;
            }

            //Generate id for task
            int count = _appTasks.Count;
            int id;

            if (count == 0)
            {
                id = 1;
            }
            else
            {
                id = _appTasks[_appTasks.Count - 1].Id + 1;
            }

            DateTime dateTime = DateTime.UtcNow;

            Task task = new Task()
            {
                Id = id,
                CreatorId = user.Id,
                CreatedAt = dateTime,
                Title = title,
                Description = description,
                IsComplete = isComplete,
                LastEditorId = user.Id,
                LastChangeAt = dateTime,
                ListId = todoListId
            };

            _appTasks.Add(task);
            SaveToFile();

            return true;
        }

        //Edit Task
        public bool EditTask(User user,int id, int newId,string newTitle, string newDescription, bool isComplete)
        {
            Task task = GetTaskById(id);

            if (_appTasks.Any(t => t.Id == newId))
            {
                return false;
            }

            task.Id = newId;
            task.Title = newTitle;
            task.Description = newDescription;
            task.IsComplete = isComplete;
            task.LastChangeAt = DateTime.UtcNow;
            task.LastEditorId = user.Id;

            SaveToFile();
            return true;
        }

        //Delete Task
        public bool DeleteTask(int id)
        {
            Task task = GetTaskById(id);

            if (task == null)
            {
                return false;
            }

            _appTasks.Remove(task);
            SaveToFile();
            return true;
        }

        //Complete Task
        public bool CompleteTask(int id)
        {
            Task task = GetTaskById(id);

            if (task == null)
            {
                return false;
            }

            task.IsComplete = true;
            SaveToFile();
            return true;
        }
    }
}
