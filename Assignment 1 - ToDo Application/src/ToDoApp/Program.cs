﻿using System;
using System.Linq;
using System.Collections.Generic;

using ToDoApp.Entities;
using ToDoApp.Services;
using ToDoApp.Entities.Enums;

namespace ToDoApp
{
    class Program
    {
        private static UserService _userService = new UserService();
        private static TaskService _taskService = new TaskService();
        private static ToDoListService _toDoListService = new ToDoListService();

        static void Main(string[] args)
        {
            bool shouldExit = false;

            while (!shouldExit)
            {
                shouldExit = MainMenu();
            }
        }

        public static bool MainMenu()
        {
            RenderMainMenu();

            string command = Console.ReadLine();

            User currentUser = _userService.CurrentUser;

            if (command == "1")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    LogOut();
                }
                return false;
            }
            else if (command == "2")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else if (currentUser.Role == Role.Admin)
                {
                    RenderUsersManagementView();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Only User with admin privileges can access.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                return false;
            }
            else if (command == "3")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    RenderToDoListManagementView();
                }
                return false;
            }
            else if (command == "4")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    RenderTasksManagementView();
                }
                return false;
            }
            else if (command == "5")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else if (currentUser.Role == Role.Admin)
                {
                    CreateUser();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Only User with admin privileges can access.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                return false;
            }
            else if (command == "6")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else if (currentUser.Role == Role.Admin)
                {
                    DeleteUser();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Only User with admin privileges can access.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                return false;
            }
            else if (command == "7")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else if (currentUser.Role == Role.Admin)
                {
                    EditUser();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Only User with admin privileges can access.");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                return false;
            }
            else if (command == "8")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else if (currentUser.Role == Role.Admin)
                {
                    GetAllUsers();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Only User with admin privileges can access.");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                return false;
            }
            else if (command == "9")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    GetCreatedByUserLists();
                }
                return false;
            }
            else if (command == "10")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    GetAllToDoLists();
                }
                return false;
            }
            else if (command == "11")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    CreateToDoList();
                }
                return false;
            }
            else if (command == "12")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    EditToDoList();
                }
                return false;
            }
            else if (command == "13")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    DeleteToDoList();
                }
                return false;
            }
            else if (command == "14")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    ShareToDoList();
                }
                return false;
            }
            else if (command == "15")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    GetAllTasks();
                }
                return false;
            }
            else if (command == "16")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    CreateTask();
                }
                return false;
            }
            else if (command == "17")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    EditTask();
                }
                return false;
            }
            else if (command == "18")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    DeleteTask();
                }
                return false;
            }
            else if (command == "19")
            {
                if (currentUser == null)
                {
                    LogIn();
                }
                else
                {
                    CompleteTask();
                }
                return false;
            }
            else if (command == "x")
            {
                return true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Command!");
                Console.ForegroundColor = ConsoleColor.White;
                return false;
            }

        }

        //Display Main Menu
        public static void RenderMainMenu()
        {
            Console.WriteLine("<--- Main Menu --->");

            if (_userService.CurrentUser == null)
            {
                Console.WriteLine("1. Login");
            }
            else
            {
                Console.WriteLine("1. LogOut");
            }

            Console.WriteLine("2. Users Management View");
            Console.WriteLine("3. ToDo List Management View");
            Console.WriteLine("4. Tasks Management View ");
            Console.WriteLine("x  Exit");
        }

        //Display Users Management View
        public static void RenderUsersManagementView()
        {
            Console.WriteLine("<--- Users Management View --->");
            Console.WriteLine("5. Add User");
            Console.WriteLine("6. Delete User");
            Console.WriteLine("7. Edit User");
            Console.WriteLine("8. List All Users");
        }

        //Display ToDo List Management View 
        public static void RenderToDoListManagementView()
        {
            Console.WriteLine("<--- ToDo List Management View --->");
            Console.WriteLine("9.  Open My ToDo Lists");
            Console.WriteLine("10. Open All ToDo Lists");
            Console.WriteLine("11. Create ToDo List");
            Console.WriteLine("12. Edit ToDo List");
            Console.WriteLine("13. Delete ToDo List");
            Console.WriteLine("14. Share ToDo List");
        }

        //Display ToDo List Management View 
        public static void RenderTasksManagementView()
        {
            Console.WriteLine("<--- Task List Management View --->");
            Console.WriteLine("15. Open My Tasks From ToDo List");
            Console.WriteLine("16. Create Task");
            Console.WriteLine("17. Edit Task");
            Console.WriteLine("18. Delete Task");
            Console.WriteLine("19. Complete Task");
        }

        public static void CompleteTask()
        {
            User currentUser = _userService.CurrentUser;

            if (currentUser == null)
            {
                Console.WriteLine("Please sign in first.");
            }
            else
            {
                Console.WriteLine("Enter Task Id:");
                int id = int.Parse(Console.ReadLine());

                bool isValid = _taskService.CompleteTask(id);

                if (isValid)
                {
                    Console.WriteLine("Task is completed");
                }
                else
                {
                    Console.WriteLine("There isn't task with this id");
                    CompleteTask();
                }
            }
        }

        public static void DeleteTask()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter Task Id:");
            int id = int.Parse(Console.ReadLine());

            var isValid = _taskService.DeleteTask(id);

            if (isValid)
            {
                Console.WriteLine("Task has been deleted.");
            }
            else
            {
                Console.WriteLine("Task with this id doen't exist.");
                DeleteTask();
            }
        }

        public static void EditTask()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter Task Id:");
            int id = int.Parse(Console.ReadLine());

            if (_taskService.GetTaskById(id) == null)
            {
                Console.WriteLine("There isn't task with this id");
                EditTask();
            }
            else
            {
                Console.WriteLine("Enter Task New Id:");
                int newId = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter task new title:");
                string newTitle = Console.ReadLine();

                Console.WriteLine("Enter task new description:");
                string newDescription = Console.ReadLine();

                Console.WriteLine("Is task complet type yes or no");
                string answer = Console.ReadLine();
                bool isComplete = false;

                if (answer == "yes")
                {
                    isComplete = true;
                }
                else
                {
                    Console.WriteLine("Invalid answer");
                    CreateTask();
                }

                bool isValid = _taskService.EditTask(currentUser, id, newId, newTitle, newDescription, isComplete);

                if (isValid == false)
                {
                    Console.WriteLine("Task with this id already exists");
                    EditTask();
                }
                else
                {
                    Console.WriteLine("Task has been edited");
                }
            }
        }

        public static void CreateTask()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter ToDo List Id:");
            int listId = int.Parse(Console.ReadLine());

            ToDoList list = _toDoListService.GetListById(listId);

            if (list == null)
            {
                Console.WriteLine("There isn't ToDo list with that id.");
            }
            else
            {
                Console.WriteLine("Enter task title:");
                string title = Console.ReadLine();

                Console.WriteLine("Enter task description:");
                string description = Console.ReadLine();

                Console.WriteLine("Is task complet type yes or no");
                string answer = Console.ReadLine();
                bool isComplete = false;

                if (answer == "yes")
                {
                    isComplete = true;
                }
                else if (answer == "no")
                {
                    isComplete = false;
                }
                else
                {
                    Console.WriteLine("Invalid answer");
                    CreateTask();
                }

                bool isValid = _taskService.CreateTask(currentUser, listId, title, description, isComplete);

                if (isValid == false)
                {
                    Console.WriteLine("Task with this title already exists");
                    CreateTask();
                }
                else
                {
                    Console.WriteLine("Task has been added");
                }
            }
        }

        public static void GetAllTasks()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter ToDo List Id:");
            int id = int.Parse(Console.ReadLine());

            ToDoList list = _toDoListService.GetListById(id);

            if (list == null)
            {
                Console.WriteLine("There isn't ToDo list with that id.");
            }
            else
            {
                if (list.CreatorId == currentUser.Id || currentUser.SharedToDoLists.Contains(list))
                {
                    List<Task> tasks = _taskService.GetAllTasks();

                    if (tasks == null)
                    {
                        Console.WriteLine("ToDo List doesn't contain any taks.");
                    }
                    else
                    {
                        foreach (var task in tasks.Where(t => t.ListId == id))
                        {
                            Console.WriteLine($"Title: {task.Title} Id: {task.Id}");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("You have no access to this ToDo List.");
                }
            }
        }

        public static void ShareToDoList()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter receiver Id here:");
            int id = int.Parse(Console.ReadLine());
            User receiver = _userService.GetUserById(id);

            if (receiver == null)
            {
                Console.WriteLine($"User with Id {id} doesn't exist");
                ShareToDoList();
            }
            else
            {
                Console.WriteLine("Enter ToDo List Id here:");
                int listid = int.Parse(Console.ReadLine());

                ToDoList list = _toDoListService.GetListById(listid);

                if (list != null)
                {
                    _userService.AddListInShare(list, receiver);
                    Console.WriteLine("ToDo List has been shared successfully.");
                }
                else
                {
                    Console.WriteLine("ToDo List with that Id doesn't exist.");
                }
            }
        }

        public static void DeleteToDoList()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter Id:");
            int id = int.Parse(Console.ReadLine());

            var isValid = _toDoListService.DeleteListById(currentUser, id);

            if (isValid)
            {
                Console.WriteLine($"ToDo List with Id {id} has been deleted.");
            }
            else
            {
                Console.WriteLine($"ToDo List with Id {id} doesn't exist.");
            }
        }

        public static void EditToDoList()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter Id:");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter New Id:");
            int newId = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter New Title:");
            string title = Console.ReadLine();

            bool isValid = _toDoListService.EditListById(id, newId, title, currentUser);

            if (isValid)
            {
                Console.WriteLine($"ToDo List has been edited.");
            }
            else
            {
                Console.WriteLine("Invalid ToDo List Id or the new Id is already taken");
            }
        }

        public static void CreateToDoList()
        {
            User currentUser = _userService.CurrentUser;

            Console.WriteLine("Enter ToDo List Title:");
            string title = Console.ReadLine();

            bool isValid = _toDoListService.CreateList(currentUser, title);

            if (isValid)
            {
                Console.WriteLine($"ToDo List with title {title} has been created.");
            }
            else
            {
                Console.WriteLine($"ToDo List with title {title} already exists.");
            }
        }

        public static void GetAllToDoLists()
        {
            List<ToDoList> lists = _toDoListService.GetAllLists();

            if (lists.Count == 0)
            {
                Console.WriteLine("There are no ToDo Lists.");
            }
            else
            {
                Console.WriteLine("Todo Lists");
                Console.WriteLine("----------");
                foreach (var list in lists)
                {
                    Console.WriteLine(list.Title);
                }
            }
        }

        public static void GetCreatedByUserLists()
        {
            User currentUser = _userService.CurrentUser;
            List<ToDoList> lists = _toDoListService.GetAllLists();

            if (lists.Count == 0)
            {
                Console.WriteLine("There are no created ToDo Lists.");
            }
            else
            {
                Console.WriteLine("My Todo Lists");
                Console.WriteLine("----------");
                foreach (var list in lists.Where(l => l.CreatorId == currentUser.Id))
                {
                    Console.WriteLine(list.Title);
                }
                foreach (var list in currentUser.SharedToDoLists)
                {
                    Console.WriteLine(list.Title);
                }
            }
        }

        private static void CreateUser()
        {
            User currentUser = _userService.CurrentUser;

            if (currentUser.Username == "admin" || currentUser.Role == Role.Admin && currentUser != null)
            {
                Console.WriteLine("Username:");
                string username = Console.ReadLine();

                Console.WriteLine("Password:");
                string password = Console.ReadLine();

                Console.WriteLine("First Name:");
                string firstName = Console.ReadLine();

                Console.WriteLine("Last Name:");
                string lastName = Console.ReadLine();

                Console.WriteLine("Select User Role:");
                Console.WriteLine("1 for Admin");
                Console.WriteLine("2 for Regular User");
                string choice = Console.ReadLine();

                Role role;
                if (choice == "1")
                {
                    role = Role.Admin;
                }
                else
                {
                    role = Role.RegularUser;
                }

                bool isValid = _userService.CreateUser(username, password, firstName, lastName, role);

                if (isValid)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"User with username: -{username}- has been created.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine("User with that name already exists!");
                    CreateUser();
                }

            }
            else
            {
                Console.WriteLine("Only user with administrative privileges can create user!");
            }
        }

        private static void DeleteUser()
        {
            User currentUser = _userService.CurrentUser;

            if (currentUser.Username == "admin" || currentUser.Role == Role.Admin && currentUser != null)
            {
                Console.WriteLine("Enter user Id:");
                int id = int.Parse(Console.ReadLine());

                bool isValid = _userService.DeleteUser(id);

                if (isValid)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"User with id -{id}- has been deleted.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine("User Id is invalid");
                    DeleteUser();
                }
            }
            else
            {
                Console.WriteLine("Only user with administrative privileges can delete another user!");
            }
        }

        private static void EditUser()
        {
            User currentUser = _userService.CurrentUser;

            if (currentUser.Username == "admin" || currentUser.Role == Role.Admin && currentUser != null)
            {
                Console.WriteLine("Enter user Id:");
                int id = int.Parse(Console.ReadLine());

                User selectedUser = _userService.GetUserById(id);

                if (selectedUser == null)
                {
                    Console.WriteLine($"There is no user with Id: {id}");
                    EditUser();
                }

                Console.WriteLine($"Username: {selectedUser.Username}");
                Console.WriteLine($"Password: {selectedUser.Password}");
                Console.WriteLine($"First Name: {selectedUser.FirstName}");
                Console.WriteLine($"Last Name: {selectedUser.LastName}");

                Console.WriteLine("Enter new username:");
                string username = Console.ReadLine();

                Console.WriteLine("Enter new password:");
                string password = Console.ReadLine();

                Console.WriteLine("Enter new First Name:");
                string firstName = Console.ReadLine();

                Console.WriteLine("Enter new Last Name:");
                string lastName = Console.ReadLine();

                bool isValid = _userService.EditUser(id, username, password, firstName, lastName);

                if (isValid)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("The changes have been made successfully");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine($"There was error in editing user profile -{username}-");
                    EditUser();
                }
            }
            else
            {
                Console.WriteLine("Only user with administrative privileges can edit user!");
            }
        }

        private static void GetAllUsers()
        {
            List<User> users = _userService.GetAllUsers();

            if (users != null)
            {
                Console.WriteLine("Application users");
                Console.WriteLine("------------------");

                foreach (var user in users)
                {
                    Console.WriteLine($"Username:{user.Username} Id:{user.Id}");
                }
            }
            else
            {
                Console.WriteLine("There are no users in application");
            }
        }

        private static void LogIn()
        {
            //Read user input
            Console.WriteLine("Enter your username:");
            string username = Console.ReadLine();

            Console.WriteLine("Enter your password:");
            string password = Console.ReadLine();

            //Check if user is successfully logged
            bool isLogged = _userService.Login(username, password);

            if (isLogged == false)
            {
                Console.WriteLine("The username doesn't exist or passowrd is wrong!");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"You are logged in as: -{_userService.CurrentUser.Username}-");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private static void LogOut()
        {
            _userService.LogOut();
        }
    }
}
